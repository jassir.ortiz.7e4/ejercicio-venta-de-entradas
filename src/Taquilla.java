import java.util.concurrent.atomic.AtomicInteger;

public class Taquilla {
    private static final AtomicInteger entradasRestantes = new AtomicInteger(10);

    public static Runnable reservarEntradas(String nombre, int numEntradas) {
        synchronized (Taquilla.class) {
            if (numEntradas > 4) {
                System.out.println(nombre + " no se puede comprar más de 4 entradas");
            } else if (entradasRestantes.get() != 0) {
                System.out.println("Compra con exito");
                entradasRestantes.set((entradasRestantes.get()-numEntradas));
            } else {
                System.out.println("No hay entradas disponibles");
            }
        }
        return null;
    }
}