import java.util.ArrayList;
import java.util.List;

public class Main {
    public static List<Runnable> users = new ArrayList<>();
    public static Thread[] threads = new Thread[7];
    private static String nombre;
    private static int numEntradas;

    public static void main(String[] args) {
        Runnable usuario1 = Taquilla.reservarEntradas("Lola21", 2);
        Runnable usuario2 = Taquilla.reservarEntradas("Pepito2", 5);
        Runnable usuario3 = Taquilla.reservarEntradas("Juan34", 4);
        Runnable usuario4 = Taquilla.reservarEntradas("Maria23", 1);
        Runnable usuario5 = Taquilla.reservarEntradas("Marta2", 3);
        Runnable usuario6 = Taquilla.reservarEntradas("BaleadasLover", 3);
        Runnable usuario7 = Taquilla.reservarEntradas("Idk555", 3);
        users.add(usuario1);
        users.add(usuario2);
        users.add(usuario3);
        users.add(usuario4);
        users.add(usuario5);
        users.add(usuario6);
        users.add(usuario7);

        for (int i = 0; i < 7; i++) {
           threads[i] = new Thread(users.get(i));
           threads[i].start();
        }
    }
}

